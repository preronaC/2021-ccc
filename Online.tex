\documentclass[dvipsnames,10pt,aspectratio=169]{beamer}

\usetheme[progressbar=foot]{metropolis}

\usepackage[english] {babel}
\input{macros}
\usepackage{amsmath,amssymb,amsfonts,graphicx}
\usepackage{commons, multirow, xfrac, xcolor}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,positioning}
\usetikzlibrary{matrix}
\usetikzlibrary{calc}
\usetikzlibrary{decorations.pathmorphing}

\title{Separating ABPs and some Structured Formulas in the Non-Commutative Setting}
%\subtitle{}

\author{\textbf{Prerona Chatterjee}}
\institute{Tata Institute of Fundamental Research, Mumbai}
\date{July 21, 2021}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}

\begin{document}
\tikzset{% 
	block/.style    = {draw, thick, rectangle, minimum height = 3em,
		minimum width = 2em},
	sum/.style      = {draw, circle, node distance = 2cm}, % Adder
	input/.style    = {coordinate}, % Input
	output/.style   = {coordinate} % Output
}
\newcommand{\suma}{\Large$+$}
\newcommand{\proda}{\Large$\times$}

\maketitle

\begin{frame}{Algebraic Circuits and Formulas}
	\begin{columns}
		\begin{column}{.4\textwidth}
		\begin{center}
			\begin{tikzpicture}[thick, node distance=2cm]
				\draw
				node [name=dummy] {}
				node [sum, right of=dummy] (top) {\suma}
				node [sum] at ($(top)+(-1,-1.5)$) (mult1) {\proda}
				node [sum] at ($(top)+(1,-1.5)$) (mult2) {\proda}
				node [sum] at ($(top)+(-1.5,-3)$) (sum1) {\suma}
				node [sum] at ($(top)+(0,-3)$) (sum2) {\suma}
				node [sum] at ($(top)+(1.5,-3)$) (sum3) {\suma}
				node [sum] at ($(top)+(-1.5,-4.5)$) (var1) {$x_1$}
				node [sum] at ($(top)+(0,-4.5)$) (var2) {$x_2$}
				node [sum] at ($(top)+(1.5,-4.5)$) (var3) {$x_3$};
				
				\draw[->](top) -- ($(top)+(0cm,1cm)$);
				\draw[->](mult1) -- node[left] {$\alpha_1$} (top);
				\draw[->](mult2) -- node[right] {$\alpha_2$} (top);
				\draw[->](sum1) -- (mult1);
				\draw[->](sum1) -- (mult2);
				\draw[->](sum2) -- (mult2);
				\draw[->](sum3) -- (mult1);
				\draw[->](var1) -- (sum1);
				\draw[->](var1) -- (sum2);
				\draw[->](var2) -- (sum1);
				\draw[->](var2) -- (sum3);
				\draw[->](var3) -- (sum2);  
				\draw[->](var3) -- (sum3); 
				
				\node at (2.5,.8) {$\ckt$};
				\uncover<3->{\node at (0,.4) {\Large{\textcolor{MidnightBlue}{$\VP$}}};}
				\uncover<5->{\node at (6,0) {\Large{\textcolor{OliveGreen}{$\boxed{\VP \supseteq \VF}$}}};}
			\end{tikzpicture}
		\end{center}
		\end{column}
		\begin{column}{.6\textwidth}
		\begin{center}
			\begin{tikzpicture}[thick, node distance=1cm]
				\uncover<2->{\draw
				node [name=dummy] {}
				node [sum, right of=dummy] (top) {\suma}
				node [sum] at ($(top)+(-1,-1.5)$) (mult1) {\proda}
				node [sum] at ($(top)+(1,-1.5)$) (mult2) {\proda}
				node [sum] at ($(top)+(-2.25,-3)$) (sum1) {\suma}
				node [sum] at ($(top)+(-.75,-3)$) (sum2) {\suma}
				node [sum] at ($(top)+(0.75,-3)$) (sum3) {\suma}
				node [sum] at ($(top)+(2.25,-3)$) (sum4) {\suma};
				\node (var1) at ($(top)+(-2.75,-4.5)$) {$x_1$};
				\node (var2) at ($(top)+(-2,-4.5)$) {$x_2$};
				\node (var3) at ($(top)+(-1.25,-4.5)$) {$x_2$};
				\node (var4) at ($(top)+(-.5,-4.5)$) {$x_3$};
				\node (var5) at ($(top)+(0.5,-4.5)$) {$x_1$};
				\node (var6) at ($(top)+(1.25,-4.5)$) {$x_2$};
				\node (var7) at ($(top)+(2,-4.5)$) {$x_1$};
				\node (var8) at ($(top)+(2.75,-4.5)$) {$x_3$};
				
				\draw[->](top) -- ($(top)+(0cm,1cm)$);
				\draw[->](mult1) -- node[left] {$\alpha_1$} (top);
				\draw[->](mult2) -- node[right] {$\alpha_2$} (top);
				\draw[->](sum1) -- (mult1);
				\draw[->](sum2) -- (mult1);
				\draw[->](sum3) -- (mult2);
				\draw[->](sum4) -- (mult2);
				\draw[->](var1) -- (sum1);
				\draw[->](var2) -- (sum1);
				\draw[->](var3) -- (sum2);
				\draw[->](var4) -- (sum2);
				\draw[->](var5) -- (sum3);
				\draw[->](var6) -- (sum3);
				\draw[->](var7) -- (sum4);
				\draw[->](var8) -- (sum4);   
				
				\node at (1.5,.8) {$\form$};}
				\uncover<4->{\node at (4,.3) {\Large{\textcolor{MidnightBlue}{$\VF$}}};}
			\end{tikzpicture}
		\end{center}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{The Non-Commutative Setting}
	\[
		f(x,y) = (x+y)^2 = (x+y) \times (x+y) = x^2 + xy + yx + y^2 \neq x^2 + 2 xy + y^2 \text{\pause}
	\]
	
	\begin{columns}
		\begin{column}{.45\textwidth}
		\begin{center}
			\begin{tikzpicture}[thick, node distance=2cm]
				\draw
				node [name=dummy] {}
				node [sum, right of=dummy] (top) {\suma}
				node [sum] at ($(top)+(-1,-1.25)$) (mult1) {\proda}
				node [sum] at ($(top)+(1,-1.25)$) (mult2) {\proda}
				node [sum] at ($(top)+(-1.5,-2.5)$) (sum1) {\suma}
				node [sum] at ($(top)+(0,-2.5)$) (sum2) {\suma}
				node [sum] at ($(top)+(1.5,-2.5)$) (sum3) {\suma}
				node [sum] at ($(top)+(-1.5,-3.75)$) (var1) {$x_1$}
				node [sum] at ($(top)+(0,-3.75)$) (var2) {$x_2$}
				node [sum] at ($(top)+(1.5,-3.75)$) (var3) {$x_3$};
				
				\draw[->](top) -- ($(top)+(0cm,1cm)$);
				\draw[->](mult1) -- node[left] {$\alpha_1$} (top);
				\draw[->](mult2) -- node[right] {$\alpha_2$} (top);
				\draw[->](sum1) -- (mult1);
				\draw[->](sum1) -- (mult2);
				\draw[->](sum2) -- (mult2);
				\draw[->](sum3) -- (mult1);
				\draw[->](var1) -- (sum1);
				\draw[->](var1) -- (sum2);
				\draw[->](var2) -- (sum1);
				\draw[->](var2) -- (sum3);
				\draw[->](var3) -- (sum2);  
				\draw[->](var3) -- (sum3); 
				
				\node at (2.5,.8) {$\ckt$};
				\node at (.5,.4) {\Large{\textcolor{MidnightBlue}{$\VP_{\text{nc}}$}}};
				\uncover<3->{\node at (6.5,0) {\Large{\textcolor{OliveGreen}{$\boxed{\VP_{\text{nc}} \supseteq \VF_{\text{nc}}}$}}};}
			\end{tikzpicture}
		\end{center}
		\end{column}
		\begin{column}{.55\textwidth}
		\begin{center}
			\begin{tikzpicture}[thick, node distance=1cm]
				\uncover<2->{\draw
				node [name=dummy] {}
				node [sum, right of=dummy] (top) {\suma}
				node [sum] at ($(top)+(-1,-1.25)$) (mult1) {\proda}
				node [sum] at ($(top)+(1,-1.25)$) (mult2) {\proda}
				node [sum] at ($(top)+(-2.25,-2.5)$) (sum1) {\suma}
				node [sum] at ($(top)+(-.75,-2.5)$) (sum2) {\suma}
				node [sum] at ($(top)+(0.75,-2.5)$) (sum3) {\suma}
				node [sum] at ($(top)+(2.25,-2.5)$) (sum4) {\suma};
				\node (var1) at ($(top)+(-2.75,-3.75)$) {$x_1$};
				\node (var2) at ($(top)+(-2,-3.75)$) {$x_2$};
				\node (var3) at ($(top)+(-1.25,-3.75)$) {$x_2$};
				\node (var4) at ($(top)+(-.5,-3.75)$) {$x_3$};
				\node (var5) at ($(top)+(0.5,-3.75)$) {$x_1$};
				\node (var6) at ($(top)+(1.25,-3.75)$) {$x_2$};
				\node (var7) at ($(top)+(2,-3.75)$) {$x_1$};
				\node (var8) at ($(top)+(2.75,-3.75)$) {$x_3$};
				
				\draw[->](top) -- ($(top)+(0cm,1cm)$);
				\draw[->](mult1) -- node[left] {$\alpha_1$} (top);
				\draw[->](mult2) -- node[right] {$\alpha_2$} (top);
				\draw[->](sum1) -- (mult1);
				\draw[->](sum2) -- (mult1);
				\draw[->](sum3) -- (mult2);
				\draw[->](sum4) -- (mult2);
				\draw[->](var1) -- (sum1);
				\draw[->](var2) -- (sum1);
				\draw[->](var3) -- (sum2);
				\draw[->](var4) -- (sum2);
				\draw[->](var5) -- (sum3);
				\draw[->](var6) -- (sum3);
				\draw[->](var7) -- (sum4);
				\draw[->](var8) -- (sum4);   
				
				\node at (1.5,.8) {$\form$};}
				\node at (4,.3) {\Large{\textcolor{MidnightBlue}{$\VF_{\text{nc}}$}}};
			\end{tikzpicture}
		\end{center}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Lower Bounds for General Non-Commutative Models}
	\begin{center}
		Is there an explicit polynomial that is outside $\VP_{\text{nc}}$? \pause
		\uncover<3->{What about $\VF_{\text{nc}}$?}
	\end{center}

	\vspace{.75em}
	\textbf{Circuits} [Str73, BS83]: $\Omega(n \log d)$ for an $n$-variate, degree $d$ polynomial. \pause \pause

	\vspace{.5em}
	\textbf{Formulas} [Nis91]: $2^{\Omega(n)}$ for a $2$-variate, degree $n$ polynomial in $\VP_{\text{nc}}$. \pause So, \textcolor{OliveGreen}{$\VF_{\text{nc}} \neq \VP_{\text{nc}}$}. \pause

	\vspace{.75em}
	\begin{center}
		But the proof is via a lower bound against non-commutative \textcolor{MidnightBlue}{Algebraic Branching Programs}.
	\end{center} \pause

	\begin{center}
		\uncover<8->{\textcolor{OliveGreen}{$\VF_{\text{nc}}$} $\subseteq$} \textcolor{MidnightBlue}{$\VBP_{\text{nc}}$} \uncover<8->{$\subseteq$ \textcolor{OliveGreen}{$\VP_{\text{nc}}$}}
	\end{center} \pause \pause

	\begin{center}
		So Nisan actually showed that $\VBP_{\text{nc}} \neq \VP_{\text{nc}}$.
	\end{center} 

	\vspace{-.5em}
\end{frame}

\begin{frame}{The ABP vs Formula Question}
	\textbf{The Question} [Nis91]:

	\vspace{-2.75em}
	\begin{center}
		Is $\VF_{\text{nc}} = \VBP_{\text{nc}}$?
	\end{center}\pause

	\vspace{.25em}
	\textbf{Note}: Every \textcolor{MidnightBlue}{monomial} in a non-commutative polynomial $f(x_1, \ldots, x_n)$ can be thought of as a \textcolor{MidnightBlue}{word} over the underlying variables $\set{x_1, \ldots, x_n}$. \pause

	\vspace{.5em}
	\textbf{Definitions}\pause
	\hspace{2em} Let $\set{X_1, \ldots, X_m}$ be a partition of the variables into buckets.\pause

	\vspace{.25em}
	\textcolor{OliveGreen}{Abecedarian Polynomials}: Polynomials in which every monomial has the form $X_1^* X_2^* \cdots X_m^*$. \pause

	\uncover<6->{
	\only<-12>{\uncover<-11>{
	\vspace{-.25em}
	\begin{center}
		$X_1 \uncover<8->{= \{x_1\}}$  
		\hspace{2em} $\uncover<7->{X_2} \uncover<8->{= \{x_2\}}$ 
		\pause \pause \pause
		\hspace{6em} \textcolor{OliveGreen}{$x_1x_1x_2$} \pause
		\hspace{2em} \textcolor{OliveGreen}{$x_2$} \pause
		\hspace{2em} \textcolor{BrickRed}{$x_1x_2x_1$}
	\end{center}}}

	\only<13->{\textcolor{OliveGreen}{Syntactically Abecedarian Formulas}: Non-commutative formulas with a syntactic restriction that makes them naturally compute abecedarian polynomials.}}
	

	\vspace{.25em}
	\uncover<14->{\begin{center}
		\textbf{Main Result}:\\
		\textcolor{BrickRed}{There is a tight superpolynomial separation between \emph{abecedarian} formulas and ABPs.}
	\end{center}}

	\vspace{-1.5em}
\end{frame}

\begin{frame}{Proof Idea}
	\textbf{The Statement}:
	There is an explicit $n^2$-variate, degree-$d$ abcd-polynomial $f_{n,d}(\textbf{x})$ such that \pause
	\begin{itemize}
		\item \textcolor{OliveGreen}{abcd-ABP Upper Bound}: the abcd-ABP complexity of $f_{n, d}(\textbf{x})$ is $\Theta(nd)$;\pause
		\item \textcolor{BrickRed}{abcd-Formula Lower Bound}: the abcd-formula complexity of $f_{n, \log n}(\textbf{x})$ is $n^{\Theta(\log \log n)}$.\pause
	\end{itemize}

	\vspace*{1.5em}
	\textbf{The Proof Idea}:
	\begin{enumerate}
		\uncover<6->{\item Use \textcolor{MidnightBlue}{low degree} to make the abcd-formula structured.}
		\uncover<7->{\item Use the structured formula to \textcolor{MidnightBlue}{amplify degree} while keeping the structure intact.}
		\uncover<8->{\item Convert the structured abcd-formula into a \textcolor{MidnightBlue}{homogeneous multilinear formula}.}
		\uncover<5->{\item Use \textcolor{MidnightBlue}{known lower bound} against homogeneous multilinear formulas [HY11].}
	\end{enumerate}
\end{frame}

\begin{frame}{Concluding Remarks}
	\textbf{Studying abcd-formulas might be enough}: For any $n$-variate polynomial that is abecedarian with respect to a partition of size $O(\log n)$, if it can be computed efficiently by formulas, then they can also be computed efficiently by abcd-formulas.\pause

	\vspace{1em}
	\textbf{Super-polynomial separation between homogeneous formulas and ABPs }[LST21]:\\
	There is an $n^2$-variate degree-$n$ polynomial that can be computed by a homogeneous ABP of size $\Theta(n^2)$ but any homogeneous formula computing it must have size $n^{\Omega(\log \log n)}$.\pause

	\vspace{2em}
	\begin{center}
		\textbf{Question}: Can ideas from [LST21] and this paper be combined to answer Nisan's question? 
	\end{center}
\end{frame}

\begin{frame}
	\begin{center}
		\textbf{\large{Thank You !}}\\
		\vspace{2em}
		Webpage: preronac.bitbucket.io\\

		\vspace{.5em}
		Email: prerona.ch@gmail.com
	\end{center}
\end{frame}
\end{document}